package Project2.Fall2022.Mescher;

import java.util.ArrayList;

public class StockItem extends StockComponent {
  private int id;
  private String location;
  private double price;
  private double origPrice;
  private int quantity;
  private int threshold;

  public StockItem(String name, String description, String location, double price, int quantity) {
    super(name, description);
    this.id = idCounter++;
    this.location = location;
    this.price = price;
    this.origPrice = price;
    this.quantity = quantity;
    this.threshold = -1;
  }

  public StockItem(String name, String description, String location, double price, int quantity,
      int threshold) {
    this(name, description, location, price, quantity);
    this.threshold = threshold;
  }

  @Override
  public String toString() {
    return "(#" + id + ") " + name;
  }

  @Override
  public int getID() {
    return id;
  }

  @Override
  public void setID(int id) {
    this.id = id;
  }

  @Override
  public String getLocation() {
    return location;
  }

  @Override
  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public double getPrice() {
    return price;
  }

  @Override
  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public double getOriginalPrice() {
    return origPrice;
  }

  @Override
  public int getQuantity() {
    return quantity;
  }

  @Override
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Override
  public int getThreshold() {
    return threshold;
  }

  @Override
  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }

  @Override
  public StockItem find(int id) {
    if (this.id == id) {
      return this;
    } else {
      return null;
    }
  }

  @Override
  public ArrayList<String> listItems() {
    ArrayList<String> items = new ArrayList<String>();

    items.add(this.toString());

    return items;
  }

  @Override
  public ArrayList<String> listInStock() {
    ArrayList<String> items = new ArrayList<String>();

    if (!this.outOfStock()) {
      items.add(this.toString());
    }

    return items;
  }

  @Override
  public ArrayList<String> listOutOfStock() {
    ArrayList<String> items = new ArrayList<String>();

    if (this.outOfStock()) {
      items.add(this.toString());
    }

    return items;
  }

  @Override
  public boolean outOfStock() {
    return quantity <= 0;
  }
}
