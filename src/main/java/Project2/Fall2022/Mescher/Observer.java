package Project2.Fall2022.Mescher;

public interface Observer {
  void update();

  void message(String message, MessageLevel level);
}
