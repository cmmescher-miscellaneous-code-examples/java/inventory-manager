package Project2.Fall2022.Mescher;

import java.util.ArrayList;

public class StockCategory extends StockComponent {
  private ArrayList<StockComponent> components;

  public StockCategory(String name, String description) {
    super(name, description);
    components = new ArrayList<StockComponent>();
  }

  @Override
  public void add(StockComponent stockComponent) {
    components.add(stockComponent);
    stockComponent.parent = this;
  }

  @Override
  public void remove(StockComponent stockComponent) {
    for (StockComponent component : components) {
      if (component instanceof StockCategory) {
        if (component == stockComponent) {
          components.remove(component);
          return;
        } else {
          component.remove(stockComponent);
        }
      } else if (component.getID() == stockComponent.getID()) {
        components.remove(component);
        return;
      }
    }
  }

  @Override
  public ArrayList<StockComponent> getChildCategories() {
    ArrayList<StockComponent> children = new ArrayList<StockComponent>();

    for (StockComponent component : components) {
      if (component instanceof StockCategory) {
        children.add(component);
        children.addAll(component.getChildCategories());
      }
    }

    return children;
  }

  @Override
  public int setCategory(StockCategory newCategory) {
    ArrayList<StockComponent> children = getChildCategories();
    if (children.contains(newCategory)) {
      return -1;
    } else if (newCategory == this) {
      return -2;
    } else {
      return super.setCategory(newCategory);
    }
  }

  @Override
  public double getPrice() {
    double price = 0;
    for (StockComponent component : components) {
      if (component instanceof StockCategory) {
        price += component.getPrice();
      } else {
        price += component.getPrice() * component.getQuantity();
      }
    }
    return price;
  }

  @Override
  public int getQuantity() {
    int quantity = 0;
    for (StockComponent component : components) {
      quantity += component.getQuantity();
    }
    return quantity;
  }

  @Override
  public StockItem find(int id) {
    for (StockComponent component : components) {
      if (component instanceof StockCategory) {
        StockItem found = component.find(id);
        if (found != null) {
          return found;
        }
      } else if (component.getID() == id) {
        return (StockItem) component;
      }
    }
    return null;
  }

  @Override
  public StockCategory findCategory(String name) {
    if (this.name.equals(name)) {
      return this;
    }

    for (StockComponent component : components) {
      if (component instanceof StockItem) {
        continue;
      } else if (component.getName().equals(name)) {
        return (StockCategory) component;
      } else {
        StockCategory found = component.findCategory(name);
        if (found != null) {
          return found;
        }
      }
    }
    return null;
  }

  @Override
  public ArrayList<String> listCategories() {
    ArrayList<String> categories = new ArrayList<String>();

    categories.add(this.toString());

    for (StockComponent component : components) {
      if (component instanceof StockCategory) {
        categories.addAll(component.listCategories());
      }
    }

    return categories;
  }

  @Override
  public ArrayList<String> listItems() {
    ArrayList<String> inventory = new ArrayList<String>();

    for (StockComponent component : components) {
      inventory.addAll(component.listItems());
    }

    return inventory;
  }

  @Override
  public ArrayList<String> listInStock() {
    ArrayList<String> inventory = new ArrayList<String>();

    for (StockComponent component : components) {
      inventory.addAll(component.listInStock());
    }

    return inventory;
  }

  @Override
  public ArrayList<String> listOutOfStock() {
    ArrayList<String> inventory = new ArrayList<String>();

    for (StockComponent component : components) {
      inventory.addAll(component.listOutOfStock());
    }

    return inventory;
  }

  @Override
  public boolean isEmpty() {
    return components.isEmpty();
  }
}
