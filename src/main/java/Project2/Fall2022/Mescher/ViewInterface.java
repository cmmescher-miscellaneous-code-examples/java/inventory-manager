package Project2.Fall2022.Mescher;

public interface ViewInterface extends Observer {
  void display();

  void update();

  void message(String message, MessageLevel level);
}
