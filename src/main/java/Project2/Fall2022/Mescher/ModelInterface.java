package Project2.Fall2022.Mescher;

import java.util.List;

public interface ModelInterface extends Observable {
  List<String> listInventory();

  List<String> listInventory(String category);

  List<String> listInStock();

  List<String> listInStock(String category);

  List<String> listOutOfStock();

  List<String> listCategories();

  StockComponent getInventory();

  StockItem getItem(int itemID);

  StockCategory getCategory(String name);

  double getTotalValue();

  boolean outOfStock(int itemID);

  void addItem(StockComponent item, String category);

  void removeItem(StockComponent item);

  void addCategory(StockCategory category, String parentCategory);

  void removeCategory(StockCategory category);

  void updateCategory(StockComponent component, String newCategory);

  void updateDescription(StockComponent component, String newDescription);

  void updateLocation(StockComponent item, String newLocation);

  void updateQuantity(StockComponent item, int quantityChange);

  void updatePrice(StockComponent item, double price);

  void setThreshold(StockComponent item, int threshold);

  void saveState();

  void loadState();

  void addObserver(Observer observer);

  void removeObserver(Observer observer);

  void notifyObservers();

  void notifyMessage(String message, MessageLevel level);
}
