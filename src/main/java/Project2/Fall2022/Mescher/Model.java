package Project2.Fall2022.Mescher;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Model implements ModelInterface {
  private StockComponent inventory;
  private ArrayList<Observer> observers;

  public Model() {
    inventory = new StockCategory("All Stock", "All categories of stock");
    observers = new ArrayList<Observer>();
  }

  public ArrayList<String> listInventory() {
    return inventory.listItems();
  }

  public ArrayList<String> listInventory(String category) {
    StockCategory stockCategory = inventory.findCategory(category);
    if (stockCategory != null) {
      return stockCategory.listItems();
    } else {
      notifyError("Category not found!");
      return null;
    }
  }

  public ArrayList<String> listInStock() {
    return inventory.listInStock();
  }

  public ArrayList<String> listInStock(String category) {
    StockCategory stockCategory = inventory.findCategory(category);
    if (stockCategory != null) {
      return stockCategory.listInStock();
    } else {
      notifyError("Category not found!");
      return null;
    }
  }

  public ArrayList<String> listOutOfStock() {
    return inventory.listOutOfStock();
  }

  public ArrayList<String> listCategories() {
    return inventory.listCategories();
  }

  public StockComponent getInventory() {
    return inventory;
  }

  public StockItem getItem(int id) {
    return inventory.find(id);
  }

  public StockCategory getCategory(String name) {
    return inventory.findCategory(name);
  }

  public double getTotalValue() {
    return inventory.getPrice();
  }

  public boolean outOfStock(int id) {
    StockItem item = inventory.find(id);
    if (item != null) {
      return item.outOfStock();
    } else {
      notifyError("Item not found!");
      return false;
    }
  }

  public void addItem(StockComponent item, String category) {
    StockCategory stockCategory = inventory.findCategory(category);
    StockItem stockItem = inventory.find(item.getID());
    if (stockCategory != null && stockItem == null) {
      stockCategory.add(item);
    } else {
      notifyError("Category not found or item already exists!");
      StockComponent.idCounter++;
      return;
    }
    notifyObservers();
    notifyMessage("Item added!");
  }

  public void removeItem(StockComponent item) {
    if (inventory.find(item.getID()) != null) {
      inventory.remove(item);
      notifyObservers();
      notifyMessage("Item removed!");
    } else {
      notifyError("Item not found!");
    }
  }

  public void addCategory(StockCategory category, String parentCategory) {
    StockCategory findParent = inventory.findCategory(parentCategory);
    StockCategory findCategory = inventory.findCategory(category.getName());
    if (findCategory != null) {
      notifyError("Category already exists!");
      return;
    } else if (findParent == null) {
      notifyError("Parent category not found!");
      return;
    } else {
      findParent.add(category);
      notifyObservers();
      notifyMessage("Category added!");
    }
  }

  public void removeCategory(StockCategory category) {
    if (inventory.findCategory(category.getName()) != null && category.isEmpty()) {
      inventory.remove(category);
      notifyObservers();
      notifyMessage("Category removed!");
    } else if (!category.isEmpty()) {
      notifyError("Category not empty, must be empty to remove!");
    } else {
      notifyError("Category not found!");
    }
  }

  public void updateCategory(StockComponent component, String newCategory) {
    StockCategory stockCategory = inventory.findCategory(newCategory);
    if (stockCategory != null) {
      int result = component.setCategory(stockCategory);

      if (result == 0) {
        notifyObservers();
        notifyMessage("Category updated!");
      } else if (result == -1) {
        notifyError("Parent category cannot be one of its children!");
      } else {
        notifyError("Parent category cannot be itself!");
      }
    } else {
      notifyError("Category not found!");
    }
  }

  public void updateDescription(StockComponent component, String newDescription) {
    component.setDescription(newDescription);
    notifyObservers();
    notifyMessage("Description updated!");
  }

  public void updateLocation(StockComponent item, String newLocation) {
    item.setLocation(newLocation);
    notifyObservers();
    notifyMessage("Location updated!");
  }

  public void updateQuantity(StockComponent item, int quantity) {
    StockItem stockItem = (StockItem) inventory.find(item.getID());
    if (quantity < 0) {
      notifyError("Quantity cannot be negative!");
      return;
    }

    if (stockItem != null) {
      stockItem.setQuantity(quantity);
      notifyObservers();
    } else {
      notifyError("Item not found!");
      return;
    }

    if (stockItem.getQuantity() == 0) {
      notifyWarning("Item out of stock!");
    } else if (stockItem.getQuantity() < stockItem.getThreshold()) {
      notifyWarning("Stock item " + stockItem.getName() + " is below threshold!");
    } else {
      notifyMessage("Quantity updated!");
    }
  }

  public void updatePrice(StockComponent item, double price) {
    StockItem stockItem = (StockItem) inventory.find(item.getID());
    if (price < 0) {
      notifyError("Price cannot be negative");
      return;
    }
    if (stockItem != null) {
      stockItem.setPrice(price);
      notifyObservers();
      notifyMessage("Price updated!");
    }
  }

  public void setThreshold(StockComponent item, int threshold) {
    StockItem stockItem = (StockItem) inventory.find(item.getID());
    if (threshold < -1) {
      notifyError("Invalid threshold, must be non-negative or -1 to disable!");
      return;
    }
    if (stockItem != null) {
      stockItem.setThreshold(threshold);
      notifyObservers();
    } else {
      notifyError("Item not found!");
      return;
    }

    if (stockItem.getQuantity() < stockItem.getThreshold()) {
      notifyWarning("Stock item " + stockItem.getName() + " is below threshold!");
    } else {
      notifyMessage("Threshold updated!");
    }
  }

  public void saveState() {
    try (FileOutputStream file = new FileOutputStream("inventory.ser", false);
        ObjectOutputStream out = new ObjectOutputStream(file)) {
      out.writeObject(inventory);
    } catch (IOException e) {
      notifyError("Error saving inventory!");
    }

    try (FileOutputStream file = new FileOutputStream("id.txt", false)) {
      file.write(StockComponent.idCounter);
    } catch (IOException e) {
      notifyError("Error saving ID counter!");
    }
  }

  public void loadState() {
    try (FileInputStream file = new FileInputStream("inventory.ser");
        ObjectInputStream in = new ObjectInputStream(file)) {
      inventory = (StockComponent) in.readObject();
    } catch (IOException e) {
      notifyError("Error loading inventory!");
    } catch (ClassNotFoundException e) {
      notifyError("Model Class not found!");
    }

    try (FileInputStream file = new FileInputStream("id.txt")) {
      StockComponent.idCounter = file.read();
    } catch (IOException e) {
      notifyError("Error loading id counter!");
    }

    observers = new ArrayList<>();
  }

  public void addObserver(Observer observer) {
    observers.add(observer);
  }

  public void removeObserver(Observer observer) {
    observers.remove(observer);
  }

  public void notifyObservers() {
    for (Observer observer : observers) {
      observer.update();
    }
  }

  private void notifyMessage(String message) {
    for (Observer observer : observers) {
      observer.message(message, MessageLevel.INFO);
    }
  }

  private void notifyWarning(String message) {
    for (Observer observer : observers) {
      observer.message(message, MessageLevel.WARNING);
    }
  }

  private void notifyError(String message) {
    for (Observer observer : observers) {
      observer.message(message, MessageLevel.ERROR);
    }
  }

  public void notifyMessage(String message, MessageLevel level) {
    switch (level) {
      case INFO:
        notifyMessage(message);
        break;
      case WARNING:
        notifyWarning(message);
        break;
      case ERROR:
        notifyError(message);
        break;
    }
  }
}
