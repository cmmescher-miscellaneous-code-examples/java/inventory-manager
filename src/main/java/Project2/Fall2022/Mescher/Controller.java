package Project2.Fall2022.Mescher;

import java.io.File;

public class Controller implements ControllerInterface {
  private ModelInterface model;
  private ViewInterface view;

  public Controller() {
    this.model = new Model();
    loadState();
    this.view = new View(model, this);
  }

  private void loadState() {
    File state = new File("inventory.ser");
    File idCounter = new File("id.txt");
    if (state.exists() && idCounter.exists()) {
      model.loadState();
    }
  }

  public void start() {
    view.display();
  }

  public void addItem(String name, String description, String location, double price, int quantity, String category,
      int threshold) {
    if (name == null || name.isEmpty()) {
      model.notifyMessage("Name cannot be empty!", MessageLevel.ERROR);
      return;
    } else if (price < 0) {
      model.notifyMessage("Price cannot be negative!", MessageLevel.ERROR);
      return;
    } else if (quantity < 0) {
      model.notifyMessage("Quantity cannot be negative!", MessageLevel.ERROR);
      return;
    } else if (category == null || category.isEmpty()) {
      model.notifyMessage("Category cannot be empty!", MessageLevel.ERROR);
      return;
    } else if (threshold < -1) {
      model.notifyMessage("Threshold must be non-negative or -1 to disable!", MessageLevel.ERROR);
      return;
    }

    if (category.equals("None")) {
      category = "All Stock";
    }
    StockItem item = new StockItem(name, description, location, price, quantity);
    item.setThreshold(threshold);
    model.addItem(item, category);
  }

  public void addItem(String name, String description, String location, double price, int quantity, String category) {
    addItem(name, description, location, price, quantity, category, -1);
  }

  public void removeItem(int id) {
    StockItem item = model.getItem(id);

    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.removeItem(item);
  }

  public void addCategory(String name, String description, String parentCategory) {
    if (name == null || name.isEmpty()) {
      model.notifyMessage("Category name cannot be empty!", MessageLevel.ERROR);
      return;
    } else if (model.getCategory(name) != null) {
      model.notifyMessage("Category already exists!", MessageLevel.ERROR);
      return;
    }

    if (parentCategory.equals("None")) {
      parentCategory = "All Stock";
    }
    StockCategory category = new StockCategory(name, description);
    model.addCategory(category, parentCategory);
  }

  public void removeCategory(String name) {
    if (name == null || name.isEmpty()) {
      model.notifyMessage("Category name cannot be empty!", MessageLevel.ERROR);
      return;
    }

    StockCategory category = model.getCategory(name);
    if (category == null) {
      model.notifyMessage("Category does not exist!", MessageLevel.ERROR);
      return;
    }

    model.removeCategory(category);
  }

  public void updateCategory(String categoryName, String newCategory) {
    if (categoryName == null || categoryName.isEmpty()) {
      model.notifyMessage("Category name cannot be empty!", MessageLevel.ERROR);
      return;
    } else if (newCategory == null || newCategory.isEmpty()) {
      model.notifyMessage("New category name cannot be empty!", MessageLevel.ERROR);
      return;
    }

    StockCategory category = model.getCategory(categoryName);
    if (category == null) {
      model.notifyMessage("Category does not exist!", MessageLevel.ERROR);
      return;
    }

    model.updateCategory(category, newCategory);
  }

  public void updateCategory(int id, String newCategory) {
    if (newCategory == null || newCategory.isEmpty()) {
      model.notifyMessage("New category name cannot be empty!", MessageLevel.ERROR);
      return;
    }

    StockItem item = model.getItem(id);
    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.updateCategory(item, newCategory);
  }

  public void updateDescription(String categoryName, String newDescription) {
    if (categoryName == null || categoryName.isEmpty()) {
      model.notifyMessage("Category name cannot be empty!", MessageLevel.ERROR);
      return;
    }

    StockCategory category = model.getCategory(categoryName);
    if (category == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.updateDescription(category, newDescription);
  }

  public void updateDescription(int id, String newDescription) {
    StockItem item = model.getItem(id);
    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.updateDescription(item, newDescription);
  }

  public void updateLocation(int id, String newLocation) {
    StockItem item = model.getItem(id);
    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.updateLocation(item, newLocation);
  }

  public void updatePrice(int id, double price) {
    StockItem item = model.getItem(id);

    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.updatePrice(item, price);
  }

  public void updateQuantity(int id, int quantity) {
    StockItem item = model.getItem(id);

    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.updateQuantity(item, quantity);
  }

  public void setThreshold(int id, int threshold) {
    StockItem item = model.getItem(id);

    if (item == null) {
      model.notifyMessage("Item not found!", MessageLevel.ERROR);
      return;
    }

    model.setThreshold(item, threshold);
  }

  public void saveState() {
    model.saveState();
  }
}
