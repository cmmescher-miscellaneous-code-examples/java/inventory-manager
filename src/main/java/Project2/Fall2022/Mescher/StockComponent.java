package Project2.Fall2022.Mescher;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class StockComponent implements Serializable {
  protected String name;
  protected String description;
  protected StockCategory parent;
  public static int idCounter = 1;
  private static final long serialVersionUID = 1L;

  public StockComponent(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public String toString() {
    return name;
  }

  public void add(StockComponent stockComponent) {
  };

  public void remove(StockComponent stockComponent) {
  };

  public int getID() {
    return -1;
  };

  public void setID(int id) {
  };

  public String getName() {
    return name;
  };

  public void setName(String name) {
    this.name = name;
  };

  public String getDescription() {
    return description;
  };

  public void setDescription(String description) {
    this.description = description;
  };

  public StockCategory getParent() {
    if (parent == null) {
      return null;
    } else if (parent.getName().equals("All Stock")) {
      return null;
    } else {
      return parent;
    }
  };

  public ArrayList<StockComponent> getChildCategories() {
    return null;
  };

  public int setCategory(StockCategory newCategory) {
    parent.remove(this);
    newCategory.add(this);
    return 0;
  };

  public String getLocation() {
    return null;
  };

  public void setLocation(String location) {
  };

  public abstract double getPrice();

  public void setPrice(double price) {
  };

  public double getOriginalPrice() {
    return -1;
  };

  public abstract int getQuantity();

  public void setQuantity(int quantity) {
  };

  public int getThreshold() {
    return -1;
  };

  public void setThreshold(int threshold) {
  };

  public abstract StockItem find(int id);

  public StockCategory findCategory(String name) {
    return null;
  };

  public ArrayList<String> listCategories() {
    return null;
  };

  public abstract ArrayList<String> listItems();
  
  public abstract ArrayList<String> listInStock();

  public abstract ArrayList<String> listOutOfStock();

  public boolean outOfStock() {
    return false;
  }

  public boolean isEmpty() {
    return false;
  }
}
