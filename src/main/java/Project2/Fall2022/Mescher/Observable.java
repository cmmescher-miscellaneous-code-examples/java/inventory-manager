package Project2.Fall2022.Mescher;

public interface Observable {
  void addObserver(Observer observer);

  void removeObserver(Observer observer);

  void notifyObservers();

  void notifyMessage(String message, MessageLevel level);
}
