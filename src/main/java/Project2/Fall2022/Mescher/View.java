package Project2.Fall2022.Mescher;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class View implements ViewInterface {
  private ModelInterface model;
  private ControllerInterface controller;

  private final static Font headerFont = new Font("Arial", Font.BOLD, 20);
  private final static Font subheaderFont = new Font("Arial", Font.BOLD, 16);
  private final static Font itemFont = new Font("Arial", Font.PLAIN, 14);
  private JComboBox<String> categoryComboBox;
  private JLabel itemLabel;
  private JComboBox<String> itemComboBox;
  private JPanel infoCardPanel;
  private JLabel allStockNameLabel;
  private JComboBox<String> allStockChildCategoriesComboBox;
  private JTextArea allStockDescriptionTextArea;
  private JLabel allStockTotalPriceLabel;
  private JLabel allStockTotalQuantityLabel;
  private JLabel categoryNameLabel;
  private JLabel parentCategoryLabel;
  private JComboBox<String> childCategoriesComboBox;
  private JDialog confirmDialog;
  private JDialog fieldDialog;
  private JDialog comboDialog;
  private JDialog areaDialog;
  private JTextArea categoryDescriptionTextArea;
  private JLabel categoryTotalPriceLabel;
  private JLabel categoryTotalQuantityLabel;
  private JLabel itemIDLabel;
  private JLabel itemNameLabel;
  private JLabel itemCategoryLabel;
  private JTextArea itemDescriptionTextArea;
  private JLabel itemLocationLabel;
  private JLabel itemOriginalPriceLabel;
  private JLabel itemPriceLabel;
  private JLabel itemQuantityLabel;
  private JLabel itemThresholdLabel;
  private JTextField addCategoryNameTextField;
  private JComboBox<String> addCategoryParentCategoryComboBox;
  private JTextArea addCategoryDescriptionTextArea;
  private JTextField addItemNameTextField;
  private JComboBox<String> addItemCategoryComboBox;
  private JTextArea addItemDescriptionTextArea;
  private JTextField addItemLocationTextField;
  private JTextField addItemPriceTextField;
  private JTextField addItemQuantityTextField;
  private JTextField addItemThresholdTextField;
  private String addCategoryCategoryName;
  private String addItemCategoryName;
  private String editItemCategoryName;
  private String editItemItemName;
  private JLabel messageLabel;

  public View(ModelInterface model, ControllerInterface controller) {
    this.model = model;
    this.controller = controller;
    this.model.addObserver(this);
  }

  public void display() {
    JFrame frame = new JFrame("Pascha Calculator");
    frame.setLayout(new GridBagLayout());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    GridBagConstraints c = new GridBagConstraints();

    // Header
    JLabel headerLabel = new JLabel("Inventory Manager");
    headerLabel.setFont(headerFont);

    // Selection Section
    JPanel selectionPanel = new JPanel();
    selectionPanel.setLayout(new GridBagLayout());
    TitledBorder selectionPanelBorder = BorderFactory.createTitledBorder("Selection:");
    selectionPanelBorder.setTitleFont(subheaderFont);
    selectionPanel.setBorder(selectionPanelBorder);

    JLabel categoryLabel = new JLabel("Category:");
    categoryLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.insets = new Insets(5, 5, 5, 5);
    selectionPanel.add(categoryLabel, c);

    categoryComboBox = new JComboBox<String>();
    categoryComboBox.setFont(itemFont);
    setCategories(categoryComboBox);
    categoryComboBox.addItemListener((e) -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        String category = categoryComboBox.getSelectedItem().toString();
        if (category.equals("Add New Category")) {
          itemLabel.setVisible(false);
          itemComboBox.setVisible(false);
          CardLayout cl = (CardLayout) (infoCardPanel.getLayout());
          cl.show(infoCardPanel, "addCategory");
          loadAddCategoryInfo();
        } else {
          setItems(itemComboBox, categoryComboBox);
        }
      }
    });

    c.gridx = 1;
    c.insets = new Insets(5, 0, 5, 5);
    selectionPanel.add(categoryComboBox, c);

    itemLabel = new JLabel("Item:");
    itemLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 1;
    c.insets = new Insets(0, 5, 5, 5);
    selectionPanel.add(itemLabel, c);

    itemComboBox = new JComboBox<String>();
    itemComboBox.setFont(itemFont);
    setItems(itemComboBox, categoryComboBox);
    itemComboBox.addItemListener((e) -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        String category = categoryComboBox.getSelectedItem().toString();
        String item = itemComboBox.getSelectedItem().toString();

        if (item.equals("Category Info")) {
          if (category.equals(model.getInventory().getName())) {
            CardLayout cl = (CardLayout) (infoCardPanel.getLayout());
            cl.show(infoCardPanel, "allStock");
            loadAllStockInfo();
          } else if (category.equals("Out of stock")) {
            CardLayout cl = (CardLayout) (infoCardPanel.getLayout());
            cl.show(infoCardPanel, "outOfStock");
          } else {
            CardLayout cl = (CardLayout) (infoCardPanel.getLayout());
            cl.show(infoCardPanel, "category");
            loadCategoryInfo(category);

          }
        } else if (item.equals("Add New Item")) {
          CardLayout cl = (CardLayout) (infoCardPanel.getLayout());
          cl.show(infoCardPanel, "addItem");
          loadAddItemInfo(category);
        } else {
          CardLayout cl = (CardLayout) (infoCardPanel.getLayout());
          cl.show(infoCardPanel, "item");
          loadItemInfo(item);
        }
      }

    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    selectionPanel.add(itemComboBox, c);

    // Information Panel

    // All Stock Card (1/5)
    JPanel allStockCardPanel = new JPanel();
    allStockCardPanel.setLayout(new GridBagLayout());

    allStockNameLabel = new JLabel("Name: ");
    allStockNameLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    allStockCardPanel.add(allStockNameLabel, c);

    JLabel allStockChildCategoriesLabel = new JLabel("Child Categories: ");
    allStockChildCategoriesLabel.setFont(itemFont);

    c.gridy = 2;
    c.gridwidth = 1;
    allStockCardPanel.add(allStockChildCategoriesLabel, c);

    allStockChildCategoriesComboBox = new JComboBox<String>();
    allStockChildCategoriesComboBox.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    allStockCardPanel.add(allStockChildCategoriesComboBox, c);

    JLabel allStockDescriptionLabel = new JLabel("Description: ");
    allStockDescriptionLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 3;
    c.gridwidth = 2;
    c.insets = new Insets(0, 5, 5, 5);
    allStockCardPanel.add(allStockDescriptionLabel, c);

    allStockDescriptionTextArea = new JTextArea(2, 15);
    allStockDescriptionTextArea.setFont(itemFont);
    allStockDescriptionTextArea.setLineWrap(true);
    allStockDescriptionTextArea.setWrapStyleWord(true);
    allStockDescriptionTextArea.setEditable(false);
    JScrollPane allStockDescriptionScrollPane = new JScrollPane(allStockDescriptionTextArea);
    allStockDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridy = 4;
    allStockCardPanel.add(allStockDescriptionScrollPane, c);

    allStockTotalPriceLabel = new JLabel("Total Price: ");
    allStockTotalPriceLabel.setFont(itemFont);

    c.gridy = 5;
    allStockCardPanel.add(allStockTotalPriceLabel, c);

    allStockTotalQuantityLabel = new JLabel("Total Quantity: ");
    allStockTotalQuantityLabel.setFont(itemFont);

    c.gridy = 6;
    allStockCardPanel.add(allStockTotalQuantityLabel, c);

    loadAllStockInfo();

    // Out of Stock Card (2/6)
    JPanel outOfStockCardPanel = new JPanel();
    outOfStockCardPanel.setLayout(new GridBagLayout());

    JLabel outOfStockNameLabel = new JLabel("Name: Out of Stock");
    outOfStockNameLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    outOfStockCardPanel.add(outOfStockNameLabel, c);

    JLabel outOfStockDescriptionLabel = new JLabel("Description: ");
    outOfStockDescriptionLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 2;
    c.insets = new Insets(0, 5, 5, 5);
    outOfStockCardPanel.add(outOfStockDescriptionLabel, c);

    JTextArea outOfStockDescriptionTextArea = new JTextArea(2, 15);
    outOfStockDescriptionTextArea.setFont(itemFont);
    outOfStockDescriptionTextArea.setLineWrap(true);
    outOfStockDescriptionTextArea.setWrapStyleWord(true);
    outOfStockDescriptionTextArea.setEditable(false);
    outOfStockDescriptionTextArea.setText("All out of stock items");
    JScrollPane outOfStockDescriptionScrollPane = new JScrollPane(outOfStockDescriptionTextArea);
    outOfStockDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridy = 3;
    outOfStockCardPanel.add(outOfStockDescriptionScrollPane, c);

    // Category Card (3/6)
    JPanel categoryCardPanel = new JPanel();
    categoryCardPanel.setLayout(new GridBagLayout());

    categoryNameLabel = new JLabel("Name: ");
    categoryNameLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    categoryCardPanel.add(categoryNameLabel, c);

    parentCategoryLabel = new JLabel("Parent Category: ");
    parentCategoryLabel.setFont(itemFont);

    c.gridy = 1;
    c.gridwidth = 1;
    c.insets = new Insets(0, 5, 5, 5);
    categoryCardPanel.add(parentCategoryLabel, c);

    JButton categoryEditParentCategoryButton = new JButton("Edit");
    categoryEditParentCategoryButton.setFont(itemFont);
    categoryEditParentCategoryButton.addActionListener((e) -> {
      JComboBox<String> dialogComboBox = new JComboBox<String>();
      String currCategory = categoryNameLabel.getText().substring(categoryNameLabel.getText().indexOf(" ") + 1);
      String currItem = parentCategoryLabel.getText().substring(parentCategoryLabel.getText().indexOf(":") + 2);
      comboDialog = createComboDialog(frame, "Select Parent Category", "Parent Category:", dialogComboBox,
          "Confirm", "Cancel", currCategory, currItem,
          (ev) -> {
            String category = dialogComboBox.getSelectedItem().toString();
            if (category.equals(model.getInventory().getName())) {
              message("Cannot assign a parent category to " + model.getInventory().getName(), MessageLevel.ERROR);
              return;
            } else if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            controller.updateCategory(currCategory, category);
            comboDialog.dispose();
          });
      comboDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    categoryCardPanel.add(categoryEditParentCategoryButton, c);

    JLabel childCategoriesLabel = new JLabel("Child Categories: ");
    childCategoriesLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 2;
    c.insets = new Insets(0, 5, 5, 5);
    categoryCardPanel.add(childCategoriesLabel, c);

    childCategoriesComboBox = new JComboBox<String>();
    childCategoriesComboBox.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    categoryCardPanel.add(childCategoriesComboBox, c);

    JLabel categoryDescriptionLabel = new JLabel("Description: ");
    categoryDescriptionLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 3;
    c.insets = new Insets(0, 5, 5, 5);
    categoryCardPanel.add(categoryDescriptionLabel, c);

    JButton categoryEditDescriptionButton = new JButton("Edit");
    categoryEditDescriptionButton.setFont(itemFont);
    categoryEditDescriptionButton.addActionListener((e) -> {
      String origDescription = categoryDescriptionTextArea.getText();
      if (origDescription.equals("None")) {
        origDescription = "";
      }
      JTextArea dialogArea = new JTextArea(origDescription, 2, 15);
      areaDialog = createAreaDialog(frame, "Edit Description", "Description:", dialogArea, "Save", "Cancel",
          (ev) -> {
            String description = dialogArea.getText();
            String categoryName = categoryNameLabel.getText().substring(categoryNameLabel.getText().indexOf(" ") + 1);

            controller.updateDescription(categoryName, description);
            areaDialog.dispose();
          });
      areaDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    categoryCardPanel.add(categoryEditDescriptionButton, c);

    categoryDescriptionTextArea = new JTextArea(2, 15);
    categoryDescriptionTextArea.setFont(itemFont);
    categoryDescriptionTextArea.setLineWrap(true);
    categoryDescriptionTextArea.setWrapStyleWord(true);
    categoryDescriptionTextArea.setEditable(false);
    JScrollPane categoryDescriptionScrollPane = new JScrollPane(categoryDescriptionTextArea);
    categoryDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridx = 0;
    c.gridy = 4;
    c.gridwidth = 2;
    c.insets = new Insets(0, 5, 5, 5);
    categoryCardPanel.add(categoryDescriptionScrollPane, c);

    categoryTotalPriceLabel = new JLabel("Total Price: ");
    categoryTotalPriceLabel.setFont(itemFont);

    c.gridy = 5;
    categoryCardPanel.add(categoryTotalPriceLabel, c);

    categoryTotalQuantityLabel = new JLabel("Total Quantity: ");
    categoryTotalQuantityLabel.setFont(itemFont);

    c.gridy = 6;
    categoryCardPanel.add(categoryTotalQuantityLabel, c);

    JButton categoryRemoveButton = new JButton("Remove Category");
    categoryRemoveButton.setFont(itemFont);
    categoryRemoveButton.addActionListener((e) -> {
      confirmDialog = createConfirmDialog(frame, "Remove Category", "Are you sure you want to remove this category?",
          (ev) -> {
            String category = categoryComboBox.getSelectedItem().toString();
            if (category.equals("All Stock")) {
              message("Cannot remove All Stock category", MessageLevel.ERROR);
              confirmDialog.dispose();
              return;
            }
            controller.removeCategory(category);
            confirmDialog.dispose();
          });

      confirmDialog.setVisible(true);
    });

    c.gridy = 7;
    c.insets = new Insets(5, 5, 5, 5);
    categoryCardPanel.add(categoryRemoveButton, c);

    // Item Card (4/6)
    JPanel itemCardPanel = new JPanel();
    itemCardPanel.setLayout(new GridBagLayout());

    itemIDLabel = new JLabel("ID: ");
    itemIDLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.insets = new Insets(5, 5, 5, 5);
    itemCardPanel.add(itemIDLabel, c);

    itemNameLabel = new JLabel("Name: ");
    itemNameLabel.setFont(itemFont);

    c.gridy = 1;
    c.insets = new Insets(0, 5, 5, 5);
    itemCardPanel.add(itemNameLabel, c);

    itemCategoryLabel = new JLabel("Category: ");
    itemCategoryLabel.setFont(itemFont);

    c.gridy = 2;
    itemCardPanel.add(itemCategoryLabel, c);

    JButton itemEditCategoryButton = new JButton("Edit");
    itemEditCategoryButton.setFont(itemFont);
    itemEditCategoryButton.addActionListener((e) -> {
      JComboBox<String> dialogComboBox = new JComboBox<String>();
      String currItem = itemCategoryLabel.getText().substring(itemCategoryLabel.getText().indexOf(" ") + 1);
      comboDialog = createComboDialog(frame, "Select Parent Category", "Parent Category:", dialogComboBox,
          "Confirm", "Cancel", null, currItem,
          (ev) -> {
            int itemID = Integer.parseInt(itemIDLabel.getText().substring(itemIDLabel.getText().indexOf(" ") + 1));
            String category = dialogComboBox.getSelectedItem().toString();
            if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            editItemCategoryName = (model.outOfStock(itemID)) ? "Out of stock" : category;
            editItemItemName = itemComboBox.getSelectedItem().toString();
            controller.updateCategory(itemID, category);
            comboDialog.dispose();
          });
      comboDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    itemCardPanel.add(itemEditCategoryButton, c);

    JLabel itemDescriptionLabel = new JLabel("Description: ");
    itemDescriptionLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 3;
    c.insets = new Insets(0, 5, 5, 5);
    itemCardPanel.add(itemDescriptionLabel, c);

    JButton itemEditDescriptionButton = new JButton("Edit");
    itemEditDescriptionButton.setFont(itemFont);
    itemEditDescriptionButton.addActionListener((e) -> {
      String origDescription = itemDescriptionTextArea.getText();
      if (origDescription.equals("None")) {
        origDescription = "";
      }
      JTextArea dialogArea = new JTextArea(origDescription, 2, 15);
      areaDialog = createAreaDialog(frame, "Edit Description", "Description:", dialogArea, "Save", "Cancel",
          (ev) -> {
            String description = dialogArea.getText();
            int itemID = Integer.parseInt(itemIDLabel.getText().substring(itemIDLabel.getText().indexOf(" ") + 1));
            String category = itemCategoryLabel.getText().substring(itemCategoryLabel.getText().indexOf(" ") + 1);
            if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            editItemCategoryName = (model.outOfStock(itemID)) ? "Out of stock" : category;
            editItemItemName = itemComboBox.getSelectedItem().toString();
            controller.updateDescription(itemID, description);
            areaDialog.dispose();
          });
      areaDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    itemCardPanel.add(itemEditDescriptionButton, c);

    itemDescriptionTextArea = new JTextArea(2, 15);
    itemDescriptionTextArea.setFont(itemFont);
    itemDescriptionTextArea.setLineWrap(true);
    itemDescriptionTextArea.setWrapStyleWord(true);
    itemDescriptionTextArea.setEditable(false);
    JScrollPane itemDescriptionScrollPane = new JScrollPane(itemDescriptionTextArea);
    itemDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridx = 0;
    c.gridy = 4;
    c.gridwidth = 2;
    c.insets = new Insets(0, 5, 5, 5);
    itemCardPanel.add(itemDescriptionScrollPane, c);

    itemLocationLabel = new JLabel("Location: ");
    itemLocationLabel.setFont(itemFont);

    c.gridy = 5;
    c.gridwidth = 1;
    itemCardPanel.add(itemLocationLabel, c);

    JButton itemEditLocationButton = new JButton("Edit");
    itemEditLocationButton.setFont(itemFont);
    itemEditLocationButton.addActionListener((e) -> {
      String origLocation = itemLocationLabel.getText().substring(itemLocationLabel.getText().indexOf(" ") + 1);
      if (origLocation.equals("None")) {
        origLocation = "";
      }
      JTextField dialogField = new JTextField(origLocation, 15);
      areaDialog = createFieldDialog(frame, "Edit Location", "Location:", dialogField, "Save", "Cancel",
          (ev) -> {
            String location = dialogField.getText();
            int itemID = Integer.parseInt(itemIDLabel.getText().substring(itemIDLabel.getText().indexOf(" ") + 1));
            String category = itemCategoryLabel.getText().substring(itemCategoryLabel.getText().indexOf(" ") + 1);
            if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            editItemCategoryName = (model.outOfStock(itemID)) ? "Out of stock" : category;
            editItemItemName = itemComboBox.getSelectedItem().toString();

            controller.updateLocation(itemID, location);
            areaDialog.dispose();
          });
      areaDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    itemCardPanel.add(itemEditLocationButton, c);

    itemOriginalPriceLabel = new JLabel("Original Price: ");
    itemOriginalPriceLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 6;
    c.insets = new Insets(0, 5, 5, 5);
    itemCardPanel.add(itemOriginalPriceLabel, c);

    itemPriceLabel = new JLabel("Price: ");
    itemPriceLabel.setFont(itemFont);

    c.gridy = 7;
    itemCardPanel.add(itemPriceLabel, c);

    JButton itemEditPriceButton = new JButton("Edit");
    itemEditPriceButton.setFont(itemFont);
    itemEditPriceButton.addActionListener((e) -> {
      JTextField dialogField = new JTextField(
          itemPriceLabel.getText().substring(itemPriceLabel.getText().indexOf("$") + 1), 20);
      fieldDialog = createFieldDialog(frame, "Edit Price", "Enter new price:",
          dialogField, "Confirm", "Cancel",
          (ev) -> {
            double price;
            try {
              price = Double.parseDouble(dialogField.getText());
            } catch (NumberFormatException ex) {
              message("Price must be a non-negative number!", MessageLevel.ERROR);
              fieldDialog.dispose();
              return;
            }
            if (price < 0) {
              message("Price must be a non-negative number!", MessageLevel.ERROR);
              fieldDialog.dispose();
              return;
            }
            int itemID = Integer.parseInt(itemIDLabel.getText().substring(itemIDLabel.getText().indexOf(" ") + 1));
            String category = itemCategoryLabel.getText().substring(itemCategoryLabel.getText().indexOf(" ") + 1);
            if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            editItemCategoryName = (model.outOfStock(itemID)) ? "Out of stock" : category;
            editItemItemName = itemComboBox.getSelectedItem().toString();

            controller.updatePrice(itemID, price);
            fieldDialog.dispose();
          });
      fieldDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    itemCardPanel.add(itemEditPriceButton, c);

    itemQuantityLabel = new JLabel("Quantity: ");
    itemQuantityLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 8;
    c.insets = new Insets(0, 5, 5, 5);
    itemCardPanel.add(itemQuantityLabel, c);

    JButton itemEditQuantityButton = new JButton("Edit");
    itemEditQuantityButton.setFont(itemFont);
    itemEditQuantityButton.addActionListener((e) -> {
      JTextField dialogField = new JTextField(
          itemQuantityLabel.getText().substring(itemQuantityLabel.getText().indexOf(" ") + 1), 20);
      fieldDialog = createFieldDialog(frame, "Edit Quantity", "Enter new quantity:",
          dialogField, "Confirm", "Cancel",
          (ev) -> {
            int quantity;
            try {
              quantity = Integer.parseInt(dialogField.getText());
            } catch (NumberFormatException ex) {
              message("Quantity must be a non-negative integer!", MessageLevel.ERROR);
              fieldDialog.dispose();
              return;
            }
            if (quantity < 0) {
              message("Quantity must be a non-negative integer!", MessageLevel.ERROR);
              fieldDialog.dispose();
              return;
            }
            int itemID = Integer.parseInt(itemIDLabel.getText().substring(itemIDLabel.getText().indexOf(" ") + 1));
            String category = itemCategoryLabel.getText().substring(itemCategoryLabel.getText().indexOf(" ") + 1);
            if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            editItemCategoryName = (quantity <= 0) ? "Out of stock" : category;
            editItemItemName = itemComboBox.getSelectedItem().toString();

            controller.updateQuantity(itemID, quantity);
            fieldDialog.dispose();
          });
      fieldDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    itemCardPanel.add(itemEditQuantityButton, c);

    itemThresholdLabel = new JLabel("Threshold: ");
    itemThresholdLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 9;
    c.insets = new Insets(0, 5, 5, 5);
    itemCardPanel.add(itemThresholdLabel, c);

    JButton itemEditThresholdButton = new JButton("Edit");
    itemEditThresholdButton.setFont(itemFont);
    itemEditThresholdButton.addActionListener((e) -> {
      String thresholdText = itemThresholdLabel.getText().substring(itemThresholdLabel.getText().indexOf(" ") + 1);
      if (thresholdText.equals("None")) {
        thresholdText = "";
      }
      JTextField dialogField = new JTextField(thresholdText, 20);
      fieldDialog = createFieldDialog(frame, "Edit Threshold", "Enter new threshold:",
          dialogField, "Confirm", "Cancel",
          (ev) -> {
            int threshold;
            if (dialogField.getText().equals("")) {
              threshold = -1;
            } else {
              try {
                threshold = Integer.parseInt(dialogField.getText());
              } catch (NumberFormatException ex) {
                message("Threshold must be a non-negative integer, or blank to disable", MessageLevel.ERROR);
                fieldDialog.dispose();
                return;
              }
              if (threshold < 0) {
                message("Threshold must be a non-negative integer, or blank to disable", MessageLevel.ERROR);
                fieldDialog.dispose();
                return;
              }
            }
            int itemID = Integer.parseInt(itemIDLabel.getText().substring(itemIDLabel.getText().indexOf(" ") + 1));
            String category = itemCategoryLabel.getText().substring(itemCategoryLabel.getText().indexOf(" ") + 1);
            if (category.equals("None")) {
              category = model.getInventory().getName();
            }

            editItemCategoryName = (model.outOfStock(itemID)) ? "Out of stock" : category;
            editItemItemName = itemComboBox.getSelectedItem().toString();

            controller.setThreshold(itemID, threshold);
            fieldDialog.dispose();
          });
      fieldDialog.setVisible(true);
    });

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    itemCardPanel.add(itemEditThresholdButton, c);

    JButton itemRemoveButton = new JButton("Remove Item");
    itemRemoveButton.setFont(itemFont);
    itemRemoveButton.addActionListener((e) -> {
      confirmDialog = createConfirmDialog(frame, "Remove Item", "Are you sure you want to remove this item?", (ev) -> {
        String item = itemComboBox.getSelectedItem().toString();
        int itemID = Integer.parseInt(item.substring(2, item.indexOf(")")));
        controller.removeItem(itemID);
        confirmDialog.dispose();
      });

      confirmDialog.setVisible(true);
    });

    c.gridx = 0;
    c.gridy = 10;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    itemCardPanel.add(itemRemoveButton, c);
    c.gridwidth = 1;

    // Add New Category Card (5/6)
    JPanel addCategoryCardPanel = new JPanel();
    addCategoryCardPanel.setLayout(new GridBagLayout());

    JLabel addCategoryNameLabel = new JLabel("Name*: ");
    addCategoryNameLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.insets = new Insets(5, 5, 5, 5);
    addCategoryCardPanel.add(addCategoryNameLabel, c);

    addCategoryNameTextField = new JTextField(15);
    addCategoryNameTextField.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(5, 0, 5, 5);
    addCategoryCardPanel.add(addCategoryNameTextField, c);

    JLabel addCategoryDescriptionLabel = new JLabel("Description: ");
    addCategoryDescriptionLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 1;
    c.insets = new Insets(0, 5, 5, 5);
    addCategoryCardPanel.add(addCategoryDescriptionLabel, c);

    addCategoryDescriptionTextArea = new JTextArea(2, 15);
    addCategoryDescriptionTextArea.setFont(itemFont);
    addCategoryDescriptionTextArea.setLineWrap(true);
    addCategoryDescriptionTextArea.setWrapStyleWord(true);
    JScrollPane addCategoryDescriptionScrollPane = new JScrollPane(addCategoryDescriptionTextArea);
    addCategoryDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridy = 2;
    c.gridwidth = 2;
    addCategoryCardPanel.add(addCategoryDescriptionScrollPane, c);

    JLabel addCategoryParentCategoryLabel = new JLabel("Parent Category*: ");
    addCategoryParentCategoryLabel.setFont(itemFont);

    c.gridy = 3;
    c.gridwidth = 1;
    addCategoryCardPanel.add(addCategoryParentCategoryLabel, c);

    addCategoryParentCategoryComboBox = new JComboBox<>();
    addCategoryParentCategoryComboBox.setFont(itemFont);
    setParentCategories(addCategoryParentCategoryComboBox);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    addCategoryCardPanel.add(addCategoryParentCategoryComboBox, c);

    JLabel addCategoryRequiredLabel = new JLabel("* Required");
    addCategoryRequiredLabel.setFont(itemFont);
    addCategoryRequiredLabel.setForeground(Color.RED);

    c.gridx = 0;
    c.gridy = 4;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    addCategoryCardPanel.add(addCategoryRequiredLabel, c);

    JButton addCategoryButton = new JButton("Add Category");
    addCategoryButton.setFont(itemFont);
    addCategoryButton.addActionListener((e) -> {
      String name = addCategoryNameTextField.getText();
      String description = addCategoryDescriptionTextArea.getText();
      String parentCategory = addCategoryParentCategoryComboBox.getSelectedItem().toString();

      addCategoryCategoryName = name;
      controller.addCategory(name, description, parentCategory);
    });

    c.gridx = 0;
    c.gridy = 5;
    addCategoryCardPanel.add(addCategoryButton, c);

    // Add New Item Card (6/6)
    JPanel addItemCardPanel = new JPanel();
    addItemCardPanel.setLayout(new GridBagLayout());

    JLabel addItemNameLabel = new JLabel("Name*: ");
    addItemNameLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.insets = new Insets(5, 5, 5, 5);
    addItemCardPanel.add(addItemNameLabel, c);

    addItemNameTextField = new JTextField(15);
    addItemNameTextField.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(5, 0, 5, 5);
    addItemCardPanel.add(addItemNameTextField, c);

    JLabel addItemCategoryLabel = new JLabel("Category*: ");
    addItemCategoryLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 1;
    c.insets = new Insets(0, 5, 5, 5);
    addItemCardPanel.add(addItemCategoryLabel, c);

    addItemCategoryComboBox = new JComboBox<>();
    addItemCategoryComboBox.setFont(itemFont);
    setParentCategories(addItemCategoryComboBox);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    addItemCardPanel.add(addItemCategoryComboBox, c);

    JLabel addItemDescriptionLabel = new JLabel("Description: ");
    addItemDescriptionLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 2;
    c.insets = new Insets(0, 5, 5, 5);
    addItemCardPanel.add(addItemDescriptionLabel, c);

    addItemDescriptionTextArea = new JTextArea(2, 15);
    addItemDescriptionTextArea.setFont(itemFont);
    addItemDescriptionTextArea.setLineWrap(true);
    addItemDescriptionTextArea.setWrapStyleWord(true);
    JScrollPane addItemDescriptionScrollPane = new JScrollPane(addItemDescriptionTextArea);
    addItemDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridy = 3;
    c.gridwidth = 2;
    addItemCardPanel.add(addItemDescriptionScrollPane, c);

    JLabel addItemLocationLabel = new JLabel("Location: ");
    addItemLocationLabel.setFont(itemFont);

    c.gridy = 4;
    c.gridwidth = 1;
    addItemCardPanel.add(addItemLocationLabel, c);

    addItemLocationTextField = new JTextField(15);
    addItemLocationTextField.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    addItemCardPanel.add(addItemLocationTextField, c);

    JLabel addItemPriceLabel = new JLabel("Price*: ");
    addItemPriceLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 5;
    c.insets = new Insets(0, 5, 5, 5);
    addItemCardPanel.add(addItemPriceLabel, c);

    addItemPriceTextField = new JTextField(15);
    addItemPriceTextField.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    addItemCardPanel.add(addItemPriceTextField, c);

    JLabel addItemQuantityLabel = new JLabel("Quantity*: ");
    addItemQuantityLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 6;
    c.insets = new Insets(0, 5, 5, 5);
    addItemCardPanel.add(addItemQuantityLabel, c);

    addItemQuantityTextField = new JTextField(15);
    addItemQuantityTextField.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    addItemCardPanel.add(addItemQuantityTextField, c);

    JLabel addItemThresholdLabel = new JLabel("Threshold: ");
    addItemThresholdLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 7;
    c.insets = new Insets(0, 5, 5, 5);
    addItemCardPanel.add(addItemThresholdLabel, c);

    addItemThresholdTextField = new JTextField(15);
    addItemThresholdTextField.setFont(itemFont);

    c.gridx = 1;
    c.insets = new Insets(0, 0, 5, 5);
    addItemCardPanel.add(addItemThresholdTextField, c);

    JLabel addItemRequiredLabel = new JLabel("* required");
    addItemRequiredLabel.setFont(itemFont);
    addItemRequiredLabel.setForeground(Color.RED);

    c.gridx = 0;
    c.gridy = 8;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    addItemCardPanel.add(addItemRequiredLabel, c);

    JButton addItemButton = new JButton("Add Item");
    addItemButton.setFont(itemFont);
    addItemButton.addActionListener((e) -> {
      String name = addItemNameTextField.getText();
      String category = addItemCategoryComboBox.getSelectedItem().toString();
      String description = addItemDescriptionTextArea.getText();
      String location = addItemLocationTextField.getText();
      double price;
      try {
        price = Double.parseDouble(addItemPriceTextField.getText());
        if (price < 0) {
          message("Price must be a non-negative number", MessageLevel.ERROR);
          return;
        }
      } catch (NumberFormatException ex) {
        message("Price must be a non-negative number", MessageLevel.ERROR);
        return;
      }
      int quantity;
      try {
        quantity = Integer.parseInt(addItemQuantityTextField.getText());
        if (quantity < 0) {
          message("Quantity must be a non-negative integer", MessageLevel.ERROR);
          return;
        }
      } catch (NumberFormatException ex) {
        message("Quantity must be a non-negative integer", MessageLevel.ERROR);
        return;
      }
      int threshold;
      if (addItemThresholdTextField.getText().equals("")) {
        threshold = -1;
      } else {
        try {
          threshold = Integer.parseInt(addItemThresholdTextField.getText());
          if (threshold < 0) {
            message("Threshold must be a non-negative integer, or blank to disable", MessageLevel.ERROR);
            return;
          }
        } catch (NumberFormatException ex) {
          message("Threshold must be a non-negative integer, or blank to disable", MessageLevel.ERROR);
          return;
        }
      }

      addItemCategoryName = (quantity <= 0) ? "Out of stock" : category;
      controller.addItem(name, description, location, price, quantity, category, threshold);
    });

    c.gridx = 0;
    c.gridy = 9;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    addItemCardPanel.add(addItemButton, c);

    // Card changer
    infoCardPanel = new JPanel();
    infoCardPanel.setLayout(new CardLayout());
    TitledBorder infoCardPanelBorder = BorderFactory.createTitledBorder("Information:");
    infoCardPanelBorder.setTitleFont(itemFont);
    infoCardPanel.setBorder(infoCardPanelBorder);
    infoCardPanel.add(allStockCardPanel, "allStock");
    infoCardPanel.add(outOfStockCardPanel, "outOfStock");
    infoCardPanel.add(categoryCardPanel, "category");
    infoCardPanel.add(itemCardPanel, "item");
    infoCardPanel.add(addCategoryCardPanel, "addCategory");
    infoCardPanel.add(addItemCardPanel, "addItem");

    // Save button
    JButton saveButton = new JButton("Save Inventory");
    saveButton.setFont(itemFont);
    saveButton.addActionListener((e) -> {
      controller.saveState();
      message("Saved", MessageLevel.INFO);
    });

    // Message Section
    JPanel messagePanel = new JPanel();
    messagePanel.setLayout(new GridBagLayout());
    TitledBorder messagePanelBorder = BorderFactory.createTitledBorder("Messages:");
    messagePanelBorder.setTitleFont(itemFont);
    messagePanel.setBorder(messagePanelBorder);

    c.gridx = 0;
    c.gridy = 0;
    messagePanel.add(Box.createRigidArea(new Dimension(250, 0)), c);

    messageLabel = new JLabel("");
    messageLabel.setFont(itemFont);

    c.gridy = 1;
    c.insets = new Insets(5, 5, 5, 5);
    c.anchor = GridBagConstraints.LINE_START;
    messagePanel.add(messageLabel, c);

    // Adding elements to main frame
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.weightx = 0.0;
    c.weighty = 0.0;
    c.anchor = GridBagConstraints.CENTER;
    frame.add(headerLabel, c);
    c.gridy = 1;
    frame.add(selectionPanel, c);
    c.gridy = 2;
    c.weighty = 1.0;
    frame.add(infoCardPanel, c);
    c.gridy = 3;
    c.weighty = 0.0;
    frame.add(saveButton, c);
    c.gridy = 4;
    frame.add(messagePanel, c);
    frame.setSize(450, 650);
    frame.setVisible(true);
  }

  private void setCategories(JComboBox<String> categoryComboBox) {
    categoryComboBox.removeAllItems();
    for (String category : model.listCategories()) {
      categoryComboBox.addItem(category);
    }
    categoryComboBox.insertItemAt("Out of stock", 1);
    categoryComboBox.insertItemAt("Add New Category", 2);
  }

  private void setParentCategories(JComboBox<String> parentCategoryComboBox) {
    setCategories(parentCategoryComboBox);
    parentCategoryComboBox.removeItemAt(0); // Remove "All Stock"
    parentCategoryComboBox.removeItemAt(0); // Remove "Out of stock"
    parentCategoryComboBox.removeItemAt(0); // Remove "Add New Category"
    parentCategoryComboBox.insertItemAt("None", 0);
    parentCategoryComboBox.setSelectedIndex(0);
  }

  private void setItems(JComboBox<String> itemComboBox, JComboBox<String> categoryComboBox) {
    String category = categoryComboBox.getSelectedItem().toString();

    itemComboBox.removeAllItems();
    itemComboBox.addItem("Category Info");

    if (!category.equals("Out of stock")) {
      itemComboBox.addItem("Add New Item");
      List<String> inventory = model.listInStock(category);
      if (inventory != null) {
        for (String item : inventory) {
          itemComboBox.addItem(item);
        }
      }
    } else {
      List<String> inventory = model.listOutOfStock();
      if (inventory != null) {
        for (String item : inventory) {
          itemComboBox.addItem(item);
        }
      }
    }

    itemLabel.setVisible(true);
    itemComboBox.setVisible(true);
  }

  private void loadAllStockInfo() {
    StockComponent category = model.getInventory();
    if (category == null) {
      message("No categories found", MessageLevel.ERROR);
    } else {
      String name = category.getName();
      ArrayList<String> children = category.getChildCategories().stream()
          .map(StockComponent::getName).collect(Collectors.toCollection(ArrayList::new));
      String description = category.getDescription();
      String totalPrice = String.format("%.2f", category.getPrice());
      String totalQuantity = String.format("%d", category.getQuantity());

      allStockNameLabel.setText("Name: " + name);
      allStockChildCategoriesComboBox.removeAllItems();
      for (String child : children) {
        allStockChildCategoriesComboBox.addItem(child);
      }
      allStockDescriptionTextArea.setText(description);
      allStockTotalPriceLabel.setText("Total Price: $" + totalPrice);
      allStockTotalQuantityLabel.setText("Total Quantity: " + totalQuantity);
    }
  }

  private void loadCategoryInfo(String categoryName) {
    StockCategory category = model.getCategory(categoryName);
    if (category == null) {
      message("Category not found.", MessageLevel.ERROR);
    } else {
      String name = category.getName();
      StockCategory parent = category.getParent();
      String parentName = parent == null ? "None" : parent.getName();
      ArrayList<String> children = category.getChildCategories().stream()
          .map(StockComponent::getName).collect(Collectors.toCollection(ArrayList::new));
      String description = category.getDescription().equals("") ? "None" : category.getDescription();
      String totalPrice = String.format("%.2f", category.getPrice());
      String totalQuantity = String.format("%d", category.getQuantity());

      categoryNameLabel.setText("Name: " + name);
      parentCategoryLabel.setText("Parent Category: " + parentName);
      childCategoriesComboBox.removeAllItems();
      for (String child : children) {
        childCategoriesComboBox.addItem(child);
      }
      categoryDescriptionTextArea.setText(description);
      categoryTotalPriceLabel.setText("Total Price: $" + totalPrice);
      categoryTotalQuantityLabel.setText("Total Quantity: " + totalQuantity);
    }
  }

  private void loadItemInfo(String itemName) {
    int itemID = Integer.parseInt(itemName.substring(2, itemName.indexOf(")")));
    StockItem item = model.getItem(itemID);
    if (item == null) {
      message("Item not found.", MessageLevel.ERROR);
    } else {
      String id = String.format("%d", item.getID());
      String name = item.getName();
      String category = item.getParent() == null ? "None" : item.getParent().getName();
      String location = item.getLocation().equals("") ? "None" : item.getLocation();
      String description = item.getDescription().equals("") ? "None" : item.getDescription();
      String origPrice = String.format("%.2f", item.getOriginalPrice());
      String price = String.format("%.2f", item.getPrice());
      String quantity = String.valueOf(item.getQuantity());
      String threshold = item.getThreshold() == -1 ? "None" : String.valueOf(item.getThreshold());

      itemIDLabel.setText("ID: " + id);
      itemNameLabel.setText("Name: " + name);
      itemCategoryLabel.setText("Category: " + category);
      itemLocationLabel.setText("Location: " + location);
      itemDescriptionTextArea.setText(description);
      itemOriginalPriceLabel.setText("Original Price: $" + origPrice);
      itemPriceLabel.setText("Price: $" + price);
      itemQuantityLabel.setText("Quantity: " + quantity);
      itemThresholdLabel.setText("Threshold: " + threshold);
    }
  }

  private void loadAddItemInfo(String category) {
    addItemNameTextField.setText("");
    setParentCategories(addItemCategoryComboBox);
    try {
      addItemCategoryComboBox.setSelectedItem(category);
    } catch (IllegalArgumentException e) {
      addItemCategoryComboBox.setSelectedIndex(0);
    }
    addItemDescriptionTextArea.setText("");
    addItemLocationTextField.setText("");
    addItemPriceTextField.setText("");
    addItemQuantityTextField.setText("");
    addItemThresholdTextField.setText("");
  }

  private void loadAddCategoryInfo() {
    addCategoryNameTextField.setText("");
    setParentCategories(addCategoryParentCategoryComboBox);
    addCategoryDescriptionTextArea.setText("");
  }

  public void update() {
    if (editItemCategoryName != null && editItemItemName != null) {
      try {
        categoryComboBox.setSelectedItem(editItemCategoryName);
        itemComboBox.setSelectedItem(editItemItemName);
      } catch (IllegalArgumentException e) {
        categoryComboBox.setSelectedIndex(0);
        itemComboBox.setSelectedIndex(0);
      }
      editItemCategoryName = null;
      editItemItemName = null;
    }

    String currentCategory = categoryComboBox.getSelectedItem().toString();
    String currentItem = itemComboBox.getSelectedItem().toString();

    setCategories(categoryComboBox);
    try {
      if (addCategoryCategoryName != null) {
        categoryComboBox.setSelectedItem(addCategoryCategoryName);
      } else {
        categoryComboBox.setSelectedItem(currentCategory);
      }
    } catch (IllegalArgumentException e) {
      categoryComboBox.setSelectedIndex(0);
    } finally {
      addCategoryCategoryName = null;
    }

    setItems(itemComboBox, categoryComboBox);
    try {
      if (addItemCategoryName != null) {
        setCategories(categoryComboBox);
        categoryComboBox.setSelectedItem(addItemCategoryName);
        setItems(itemComboBox, categoryComboBox);
        itemComboBox.setSelectedIndex(itemComboBox.getItemCount() - 1);
      } else {
        itemComboBox.setSelectedItem(currentItem);
      }
    } catch (IllegalArgumentException e) {
      itemComboBox.setSelectedIndex(0);
    } finally {
      addItemCategoryName = null;
    }
  }

  private JDialog createConfirmDialog(JFrame frame, String title, String message, ActionListener listener) {
    final JDialog dialog = new JDialog(frame, title, Dialog.ModalityType.APPLICATION_MODAL);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setSize(400, 200);

    Container contentPane = dialog.getContentPane();
    contentPane.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();

    JLabel messageLabel = new JLabel(message);
    messageLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(10, 10, 10, 10);
    contentPane.add(messageLabel, c);

    JButton confirmButton = new JButton("Confirm");
    confirmButton.setFont(itemFont);
    confirmButton.addActionListener(listener);

    c.gridy = 1;
    c.gridwidth = 1;
    contentPane.add(confirmButton, c);

    JButton cancelButton = new JButton("Cancel");
    cancelButton.setFont(itemFont);
    cancelButton.addActionListener(e -> dialog.dispose());

    c.gridx = 1;
    contentPane.add(cancelButton, c);

    return dialog;
  }

  private JDialog createFieldDialog(JFrame frame, String title, String label, JTextField dialogTextField,
      String confirmText, String denyText, ActionListener action) {
    final JDialog dialog = new JDialog(frame, title, Dialog.ModalityType.DOCUMENT_MODAL);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setSize(400, 200);

    Container dialogContainer = dialog.getContentPane();
    dialogContainer.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();

    JLabel dialogLabel = new JLabel(label);
    dialogLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    dialogContainer.add(dialogLabel, c);

    dialogTextField.setFont(itemFont);

    c.gridy = 1;
    dialogContainer.add(dialogTextField, c);

    JButton confirmButton = new JButton(confirmText);
    confirmButton.setFont(itemFont);
    confirmButton.addActionListener(action);

    c.gridy = 2;
    c.gridwidth = 1;
    dialogContainer.add(confirmButton, c);

    JButton denyButton = new JButton(denyText);
    denyButton.setFont(itemFont);
    denyButton.addActionListener(e -> dialog.dispose());

    c.gridx = 1;
    dialogContainer.add(denyButton, c);

    return dialog;
  }

  private JDialog createAreaDialog(JFrame frame, String title, String label, JTextArea dialogTextArea,
      String confirmText, String denyText, ActionListener action) {
    final JDialog dialog = new JDialog(frame, title, Dialog.ModalityType.DOCUMENT_MODAL);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setSize(400, 200);

    Container dialogContainer = dialog.getContentPane();
    dialogContainer.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();

    JLabel dialogLabel = new JLabel(label);
    dialogLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    dialogContainer.add(dialogLabel, c);

    dialogTextArea.setFont(itemFont);
    dialogTextArea.setLineWrap(true);
    dialogTextArea.setWrapStyleWord(true);
    JScrollPane dialogScrollPane = new JScrollPane(dialogTextArea);
    dialogScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    c.gridy = 1;
    dialogContainer.add(dialogScrollPane, c);

    JButton confirmButton = new JButton(confirmText);
    confirmButton.setFont(itemFont);
    confirmButton.addActionListener(action);

    c.gridy = 2;
    c.gridwidth = 1;
    dialogContainer.add(confirmButton, c);

    JButton denyButton = new JButton(denyText);
    denyButton.setFont(itemFont);
    denyButton.addActionListener(e -> dialog.dispose());

    c.gridx = 1;
    dialogContainer.add(denyButton, c);

    return dialog;
  }

  private JDialog createComboDialog(JFrame frame, String title, String label, JComboBox<String> dialogComboBox,
      String confirmText, String denyText, String currCategory, String selection, ActionListener action) {
    final JDialog dialog = new JDialog(frame, title, Dialog.ModalityType.DOCUMENT_MODAL);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setSize(400, 200);

    Container dialogContainer = dialog.getContentPane();
    dialogContainer.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();

    JLabel dialogLabel = new JLabel(label);
    dialogLabel.setFont(itemFont);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 5, 5);
    dialogContainer.add(dialogLabel, c);

    dialogComboBox.setFont(itemFont);
    setParentCategories(dialogComboBox);
    if (currCategory != null) {
      try {
        dialogComboBox.removeItem(currCategory);
      } catch (Exception e) {
        System.out.println("Error removing current category from combo box");
      }
    }
    try {
      dialogComboBox.setSelectedItem(selection);
    } catch (IllegalArgumentException e) {
      dialogComboBox.setSelectedIndex(0);
    }

    c.gridy = 1;
    dialogContainer.add(dialogComboBox, c);

    JButton confirmButton = new JButton(confirmText);
    confirmButton.setFont(itemFont);
    confirmButton.addActionListener(action);

    c.gridy = 2;
    c.gridwidth = 1;
    dialogContainer.add(confirmButton, c);

    JButton denyButton = new JButton(denyText);
    denyButton.setFont(itemFont);
    denyButton.addActionListener(e -> dialog.dispose());

    c.gridx = 1;
    dialogContainer.add(denyButton, c);

    return dialog;
  }

  public void message(String message, MessageLevel level) {
    messageLabel.setText(message);
    if (level == MessageLevel.ERROR) {
      messageLabel.setForeground(Color.RED);
    } else if (level == MessageLevel.WARNING) {
      messageLabel.setForeground(new Color(228, 142, 44));
    } else {
      messageLabel.setForeground(Color.BLACK);
    }
  }
}
