package Project2.Fall2022.Mescher;

public interface ControllerInterface {
  void start();

  void addItem(String name, String description, String location, double price, int quantity, String category,
      int threshold);

  void addItem(String name, String description, String location, double price, int quantity, String category);

  void removeItem(int id);

  void addCategory(String name, String description, String parentCategory);

  void removeCategory(String name);

  void updateCategory(String categoryName, String newCategory);

  void updateCategory(int id, String newCategory);

  void updateDescription(String categoryName, String newDescription);

  void updateDescription(int id, String newDescription);

  void updateLocation(int id, String newLocation);

  void updatePrice(int id, double price);

  void updateQuantity(int id, int quantity);

  void setThreshold(int id, int threshold);

  void saveState();
}
